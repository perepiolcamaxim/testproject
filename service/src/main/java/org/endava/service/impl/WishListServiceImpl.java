package org.endava.service.impl;

import lombok.RequiredArgsConstructor;
import org.endava.persistence.entity.Item;
import org.endava.persistence.entity.WishList;
import org.endava.persistence.repository.WishListRepository;
import org.endava.service.WishListService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class WishListServiceImpl implements WishListService {

    private final WishListRepository wishListRepository;

    @Override
    public List<WishList> findAll() {
        return wishListRepository.findAll();
    }

    @Override
    public WishList findById(Long id) {
        return wishListRepository.findById(id).orElseThrow( ()-> new NoSuchElementException("No sush wishlist"));
    }

    @Override
    public void deleteById(Long id) {
        wishListRepository.deleteById(id);
    }

    @Override
    public WishList add(WishList wishList) {
        return wishListRepository.save(wishList);
    }

    @Override
    public WishList addItem(Long id, Item item) {
        WishList wishList = wishListRepository.findById(id).orElseThrow(()-> new NoSuchElementException("No such wishlist"));

        wishList.addItem(item);
        return wishListRepository.save(wishList);
    }

    @Override
    public void update(WishList wishList) {
        wishListRepository.save(wishList);
    }
}
