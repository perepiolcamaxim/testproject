package org.endava.service.impl;

import org.endava.persistence.entity.Item;
import org.endava.service.ItemService;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {
    @Override
    public List<Item> findAll() {
        return null;
    }

    @Override
    public Item findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public Item add(Item wishList) {
        return null;
    }

    @Override
    public void update(Item wishList) {

    }
}
