package org.endava.service;

import org.endava.persistence.entity.Item;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ItemService {
    List<Item> findAll();
    Item findById(Long id);
    void deleteById(Long id);
    Item add(Item wishList);
    void update(Item wishList);
}
