package org.endava.service;

import org.endava.persistence.entity.Item;
import org.endava.persistence.entity.WishList;
import java.util.List;

public interface WishListService {
    List<WishList> findAll();
    WishList findById(Long id);
    void deleteById(Long id);
    WishList add(WishList wishList);
    WishList addItem(Long id, Item item);
    void update(WishList wishList);
}
