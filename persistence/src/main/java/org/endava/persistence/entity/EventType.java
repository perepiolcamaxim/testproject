package org.endava.persistence.entity;

public enum EventType {
    WEEDING, BIRTHDAY, OTHER
}
