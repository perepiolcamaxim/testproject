package org.endava.persistence.entity;

import lombok.Data;
import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Data
public class WishList {
    @Id
    @GeneratedValue
    private Long id;

    private String title;
    private Date date;
    private String description;

    @Enumerated(EnumType.STRING)
    private PrivacyType privacyType;

    @Enumerated(EnumType.STRING)
    private EventType eventType;

    @OneToMany(mappedBy = "wishList", cascade = CascadeType.ALL)
    private List<Item> items;

    public void addItem(Item item){
        items.add(item);
    }
}
