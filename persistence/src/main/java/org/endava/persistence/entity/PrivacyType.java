package org.endava.persistence.entity;

public enum PrivacyType {
    PRIVATE, NON_PRIVATE
}
