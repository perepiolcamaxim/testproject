package org.endava.persistence.entity;

import lombok.Data;
import javax.persistence.*;

@Entity
@Data
public class Item {
    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "WISH_LIST_ID")
    private WishList wishList;
}
