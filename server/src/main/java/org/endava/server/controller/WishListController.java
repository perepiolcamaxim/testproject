package org.endava.server.controller;

import lombok.RequiredArgsConstructor;
import org.endava.persistence.entity.Item;
import org.endava.persistence.entity.WishList;
import org.endava.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/wishlist")
@RequiredArgsConstructor
public class WishListController {

    private final WishListService wishListService;

    @GetMapping("/{id}")
    public WishList getWishListById(@PathVariable Long id){
       return wishListService.findById(id);
    }

    @GetMapping("")
    public List<WishList> getAll(){
        return wishListService.findAll();
    }

    @PostMapping("")
    public WishList addWishList(@RequestBody WishList wishList){
        return wishListService.add(wishList);
    }

    @PostMapping("/{id}")
    public WishList addItemToWishList(@PathVariable Long id, @RequestBody Item item){
        return wishListService.addItem(id, item);
    }

    @DeleteMapping("{id}")
    public void deleteWishList(@PathVariable Long id){
        wishListService.deleteById(id);
    }

}
